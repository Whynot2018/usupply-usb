#include "Interrupt.hpp"

extern "C"
{
	using System::InterruptSource;
	/**
	 * 
	 */
	void WWDG_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eWWDG>::Dispatch();
	}
	void RTC_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eRTC>::Dispatch();
	}
	void FLASH_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eFLASH>::Dispatch();
	}
	void EXTI0_1_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eEXTI0_1>::Dispatch();
	}
	void EXTI2_3_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eEXTI2_3>::Dispatch();
	}
	void EXTI4_15_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eEXTI4_15>::Dispatch();
	}
	void DMA1_Channel1_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eDMA_CH1>::Dispatch();
	}
	void DMA1_Channel2_3_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eDMA_CH2_3>::Dispatch();
	}
	void TIM1_BRK_UP_TRG_COM_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM1_BRK_UP_TRG_COM>::Dispatch();
	}
	void TIM1_CC_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM1_CC>::Dispatch();
	}
	void TIM3_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM3>::Dispatch();
	}
	void TIM14_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM14>::Dispatch();
	}
	void ADC1_COMP_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eADC_COMP>::Dispatch();
	}
	void TIM16_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM16>::Dispatch();
	}
	void TIM17_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM17>::Dispatch();
	}
	void I2C1_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eI2C1>::Dispatch();
	}
	void SPI1_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eSPI1>::Dispatch();
	}
	void USART1_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eUSART1>::Dispatch();
	}
	void USART2_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eUSART2>::Dispatch();
	}
	void RCC_CRS_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eRCC_CRS>::Dispatch();
	}
	void USB_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eUSB>::Dispatch();
	}
	void DMA1_Channel4_5_6_7_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eDMA_CH4_5_6_7>::Dispatch();
	}
	void PendSV_Handler()
	{
		InterruptDispatch<InterruptSource::ePendSV>::Dispatch();
	}
	void SysTick_Handler()
	{
		InterruptDispatch<InterruptSource::eSysTick>::Dispatch();
	}
#ifndef STM32F070x6
	void TIM2_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM2>::Dispatch();
	}
	void TIM6_DAC_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM6_DAC>::Dispatch();
	}
	void TIM15_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM15>::Dispatch();
	}
	void SPI2_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eSPI2>::Dispatch();
	}
	void TSC_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTSC>::Dispatch();
	}
	void TIM7_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eTIM7>::Dispatch();
	}
	void USART3_4_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eUSART3_4>::Dispatch();
	}
	void CEC_CAN_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eCEC_CAN>::Dispatch();
	}
	void I2C2_IRQHandler()
	{
		InterruptDispatch<InterruptSource::eI2C2>::Dispatch();
	}
	void PVD_VDDIO2_IRQHandler()
	{
		InterruptDispatch<InterruptSource::ePVD_VDDIO2>::Dispatch();
	}
#endif
}
