
#include "Types.hpp"
#include "Clocks.hpp"
#include "Serial.hpp"
#include "USB.hpp"
#include "Interface.hpp"
#include "USBPD.hpp"
#include "Analog.hpp"
#include <algorithm>
/**
 */
using namespace System;
using namespace Peripherals;
/**
 */
struct USBHIDInterfaceCallback;
/**
 * @brief Receive buffers for USB and USART
 */
using HIDInterface 		= USB::HIDInterface<USBHIDInterfaceCallback>;
using USARTInterface 	= USART<void(*)(Containers::DataView<uint8_t>)>;
/**
 * @brief This handles the USB HID interface on the USB device
 * 
 * The connection between this type and the below is established at compile-time. 
 */
struct USBHIDInterfaceCallback
{
	/**
	 * @brief Add all the items in the packet to the output array
	 * 
	 * Not all items in the receive data represent transmitted data, most of them
	 * will be parts of the fixed size packet which is likely mostly empty.
	 */
	void operator()( uint8_t endpoint, Containers::DataView<uint8_t> data ) noexcept
	{
		if (data.Size() > 2)
		{
			auto size = data[1];
			USARTInterface::Transmit( { data.PopFront(2).Data(), size } );
		}
	}
};
/**
 */
void USART_Receive( Containers::DataView<uint8_t> data ) noexcept
{
	HIDInterface::Transmit( 1, { data.Data(), data.Size() } );
}
/**
 * -~=< entry point >=~-
 */
int main(int argc, char* argv[])
{
	System::SystemClock_t sys_clock;
	/**
	 * This will start the USB PD enumation process.
	 */
	Peripherals::SysTickModule sys_tick
	{
		1000 ///< 1 Khz frequency or 1 ms period.
		,[](uint64_t){ USB::PD::Tick(); } ///< Update the USB_PD tick.
	};
	/**
	 * @brief This is the USB data structure, it manages the USB device
	 * 
	 * The interface template arguments for this type are only for implmentation details
	 */
	HIDInterface usb_hid{ USBHIDInterfaceCallback{} };
	USB::USBManager<HIDInterface> usb;
	/**
	 * @brief Wait for 100ms, or 100 ticks
	 */
	sys_tick.Wait( 100 );
	/**
	 * @brief This manages the USART interface.
	 * 
	 * This uses the internal DMA to buffer receives and then detects when
	 *  the receive line is idle. When idle is detected the receive callback is triggered. 
	 */
	USARTInterface serial{ USART_Receive };
	/**
	 * @brief Detect the type of charger connected
	 * 
	 * If its not a charger its a SDP, nothing special about that.
	 */
	const char * volatile current{
		[]() -> const char *
		{
			switch ( USB::USB_LL::BatteryChargeDetect<Peripherals::SysTickCommon>() )
			{
			case ModeBCD::SDP:
				return "!SOUR:POW:LIM 2.5!";
			case ModeBCD::DCP:
				/**
				 * 31211
				 * A DCP has no data lines so it will never have an enable signal.
				 */
				::System::pwr_en = true;
				[[fallthrough]];
			case ModeBCD::CDP:
				return "!SOUR:POW:LIM 7!";
			default:
				/**
				 * A non-complient charger cannot be trusted. 
				 *  do not allow high power modes on these chargers.
				 */
				return "!SOUR:POW:LIM 0.5!";
			}
		}()
	};
	const char * previous = nullptr;
	/**
	 * The USB connection is initiated here.
	 *	obviously fails 1/3 cases of above, no data avaliabe DCP.
	 */
	usb.Connect();
	/**
	 * @brief Used to store the power limit of the usb bus, only updated once.
	 */
	USB::PD::USBPD usb_pd;
	/**
	 */
	while( true )
	{
		usb_pd.Poll();
		/**
		 * Update the power limit command if type-c usb pd is enabled.
		 */
		if ( usb_pd.TypeCPowerMode() )
		{
			current = usb_pd.GetPowerLimitCommand( System::GetVbus( System::R66, System::R64 ) );
		}
		/**
		 * Send the power limit command to the main microcontroller.
		 *  as no other serial data should be sent during this period its in a critical section.
		 */
		if ( current != previous )
		{
			sys_tick.Wait(1000);
			{
				CriticalSection section;
				serial.Transmit( { (uint8_t const *)current, strlen(current) } );
				previous = current;
			}
		}
	}
	return 0;
}