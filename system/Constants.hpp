#pragma once
#include "Math.hpp"
#include <cstdint>

namespace System
{
    //
    static constexpr float 			R12 					= 10000;
    static constexpr float 			R64 					= 100000;
    static constexpr float 			R66 					= 20000;
    static constexpr float const 	AnalogReference 		= 3.3f; 	// Volts
	static constexpr unsigned		TemperatureADCChannel 	= 16u;
	//
	static constexpr size_t 		USARTQueueLength		= 512;
	static constexpr std::uint32_t	USARTBaudrate			= 19200u / 2; //The STM32f030 has a different clock into the USART intially, this should be made consistant.
	static constexpr std::size_t	USARTDMAChannel			= 1u;
    //
	constexpr std::uint32_t const LSEClock 			= 32768;
	constexpr std::uint32_t const LSIClock			= 40000;
	constexpr std::uint32_t const RTCClock			= LSEClock;
	constexpr std::uint32_t const HSIClock 			= General::MHz(8 );
	constexpr std::uint32_t const HSI14Clock		= General::MHz(14);
	constexpr std::uint32_t const HSEClock			= General::MHz(8u);
}