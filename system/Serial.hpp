#pragma once
#include "DMAUSART.hpp"
#include "DataView.hpp"
#include "PinDefinitions.hpp"
#include <array>

namespace System
{
    using ReceiveData = Containers::DataView<uint8_t>;
    //
    struct USARTKernal
    {
        using TXPin = System::Pins::UART_TX;
        using RXPin = System::Pins::UART_RX;
        //
        // Protocol
        static constexpr std::uint32_t 								Baudrate 	    = ::System::USARTBaudrate;
        static constexpr ::Peripherals::USARTGeneral::Databits 	    Databits 	    = Peripherals::USARTGeneral::Databits::Bits8;
        static constexpr ::Peripherals::USARTGeneral::Stopbits 	    Stopbits 	    = Peripherals::USARTGeneral::Stopbits::One;
        static constexpr ::Peripherals::USARTGeneral::Parity 		Parity 		    = Peripherals::USARTGeneral::Parity::None;
        static constexpr ::Peripherals::USARTGeneral::FlowControl 	FlowControl     = Peripherals::USARTGeneral::FlowControl::None;
        static constexpr std::size_t								TXDMAChannel    = 4;
        static constexpr std::size_t								RXDMAChannel    = 5;
        static constexpr std::size_t								Channel         = 2;
        static constexpr std::size_t                                BufferSize      = ::System::USARTQueueLength;
        //
        // Action
        static constexpr bool Echo      = false;
        static constexpr bool UseDMA    = false;
    };
    //
    template <typename Callback>
    using USART = Peripherals::USARTDMAModule<USARTKernal, Callback>;
}