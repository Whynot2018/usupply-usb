#pragma once
#include "ADC.hpp"
#include "PinDefinitions.hpp"

namespace System
{
    /**
     * @brief Samples the value on VBUS.
     * 
     * @return float The voltage of the vbus line.
     * 
     * The result is in volts.
     */
    inline float GetVbus(float RL, float RH) noexcept
    {
        static Peripherals::ADCModule<Pins::VBUS_ADC> adc{[](){}};
        //
        // Trigger and wait for completion.
        adc.TriggerBlock();
        //
        // Undivide the resistor divider.
        return adc.Scale * (float)adc.Get<Pins::VBUS_ADC>() * (RL + RH) / RL;
    }
}
