#pragma once
#include "USB_Types.hpp"

namespace USB
{
    #pragma pack(push, 1)
    /**
     * @brief A device qualifier descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct DeviceQualifierDescriptor : StandardHeader<DeviceQualifierDescriptor, DescriptorTypes::DEVICE_QUALIFIER>
    {
        General::Version bcdUSB;
        U<1> bDeviceClass;
        U<1> bDeviceSubClass;
        U<1> bDeviceProtocol;
        MaxPacketSize bMaxPacketSize0;
        U<1> bNumConfigurations;
        U<1> bReserved;
    };
    #pragma pack(pop)
}