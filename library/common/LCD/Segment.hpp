#pragma once

namespace LCD
{
	template <typename base_class, typename def>
	struct Segment : public base_class
	{
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr Segment() = delete;
		constexpr Segment( const Segment& ) = delete;
		constexpr Segment& operator=( const Segment& ) = delete;
		/**
		 * @brief Turn on or off a segment.
		 * 
		 * @param input The value to assign (true is on).
		 * @return Segment& For chained operations.
		 */
		constexpr Segment & Assign( bool input ) noexcept
		{
			base_class::template Set<def>( input );
			return *this;
		}
		/**
		 * @brief Assign a value to the segment.
		 * 
		 * @param input The value to assign (true is on).
		 * @return Segment& For chained operations.
		 */
		constexpr Segment & operator=( bool input ) noexcept
		{
			return Assign( input );
		}
		/**
		 * @brief Turns off the segment.
		 * 
		 * @return Segment& For chained operations.
		 */
		constexpr Segment & Clear() noexcept
		{
			return Assign( false );
		}
	};
}