#pragma once

#include "PinCommon.hpp"

namespace IO
{
	/**
	 * @brief Emulate a pin that may not in reality exist
	 * 
	 * used for things like matrix keypads where each pin is not directly connected to a specific pin. 
	 *
	 * The BitKernal template parameter must have the ability to convert to bool.
	 * 
	 * @tparam BitKernal The underlying strucutre that allows a bit to be read from the "VirtualPin".
	 */
	template <typename BitKernal>
	struct VirtualPin : private BitKernal
	{
		/**
		 * @brief A reference to the underlying BitKernal
		 * 
		 * A bitkernal could be an array of bool used to store state of a pin.
		 * 
		 * @return BitKernal& The BitKernal
		 */
		BitKernal & Kernal() noexcept
		{
			return *this;
		}
		/**
		 * @brief A reference to the underlying BitKernal
		 * 
		 * A bitkernal could be an array of bool used to store state of a pin.
		 * 
		 * @return BitKernal& The BitKernal
		 */
		BitKernal const & Kernal() const noexcept
		{
			return *this;
		}
		/**
		 * @brief Attempt to convert the kernal to a type implicity
		 * 
		 * @tparam T The type to convert to.
		 * @return T The value returned by the BitKernal when converted to the type.
		 */
		template <typename T>
		operator T() const noexcept
		{
			return Kernal();
		}
		/**
		 * @brief Read the state of the virtual pin.
		 * 
		 * @return true The state is true.
		 * @return false The state is false.
		 */
		operator bool() const noexcept
		{
			return Kernal();
		}
		/**
		 * @brief Read the state of the virtual pin.
		 * 
		 * @return High The state is true.
		 * @return Low The state is false.
		 */
		operator State() const noexcept
		{
			return ( State )(bool)Kernal();
		}
		/**
		 * @brief Attempt to assign a value to the kernal.
		 * 
		 * @tparam T The value type to try to assign.
		 * @param input The value to assign.
		 * @return VirtualPin& For chained operations. 
		 */
		template <typename T>
		VirtualPin& operator=( T input ) noexcept
		{
			Kernal() = input;
			return *this;
		}
		/**
		 * @brief Assign a bool to the kernal (for outputs).
		 * 
		 * @param input The value to assign.
		 * @return VirtualPin & For chained operations.
		 */
		VirtualPin& operator=( bool input ) noexcept
		{
			Kernal() = input;
			return *this;
		}
		/**
		 * @brief Construct a new Virtual Pin object
		 * 
		 * No parameter in this function does anything, its just for interface consistancy with the Pin class.
		 */
		template <typename ... Args>
		VirtualPin( Args... ) {}
	};
}