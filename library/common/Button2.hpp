#pragma once

#include "AtIndex.hpp"
#include "StaticLambdaWrapper.hpp"
#include <algorithm>

namespace IO
{
    /**
     * @brief An enum that defines the types of events that a button can encounter.
     */
    enum class StateEvent : std::size_t
    {
        None		=	0,
        ShortLow	=	1,
        ShortHigh	=	2,
        LongLow		=	3,
        LongHigh	=	4
    };
    /**
     * @brief This is a class that builds the properties of a Button2 kernal.
     * 
     * @tparam ShortHighCount_v The number of counts high before a ShortHigh event is raised. If zero, the event is never raised.
     * @tparam ShortLowCount_v The number of counts low before a ShortLow event is raised. If zero, the event is never raised.
     * @tparam LongHighCount_v The number of counts high before a LongHigh event is raised. If zero, the event is never raised.
     * @tparam LongLowCount_v The number of counts low before a LongLow event is raised. If zero, the event is never raised.
     */
    template <std::size_t ShortHighCount_v, std::size_t ShortLowCount_v, std::size_t LongHighCount_v, std::size_t LongLowCount_v>
    struct Button2Kernal
    {
        static constexpr std::size_t const	ShortHighCount      = ShortHighCount_v;
        static constexpr std::size_t const	ShortLowCount       = ShortLowCount_v;
        static constexpr std::size_t const	LongHighCount       = LongHighCount_v;
        static constexpr std::size_t const	LongLowCount		= LongLowCount_v;

        static constexpr std::size_t const	MaxHighCount		= (ShortHighCount > LongHighCount) ? ShortHighCount : LongHighCount;
        static constexpr std::size_t const	MaxLowCount			= ( ShortLowCount > LongLowCount ) ? ShortLowCount  : LongLowCount;

        static constexpr bool const			ShortCountEnabled   = (ShortHighCount != 0	)||(ShortLowCount != 0);
        static constexpr bool const			LongCountEnabled    = (LongHighCount != 0	)||(LongLowCount != 0);
        static constexpr bool const			HighCountEnabled    = (ShortHighCount != 0	)||(LongHighCount != 0);
        static constexpr bool const			LowCountEnabled     = (ShortLowCount != 0	)||(LongLowCount != 0);
    };
    /**
     * @brief This class stores the state of the button.
     * 
     * Internally there are two counters to achieve the details outlined in the Button2Kernal parameters.
     * 
     * @tparam Kernal The button kernal (For example a Button2Kernal instantiation)
     * @tparam Pin The pin to poll the state of. This parameter is NOT used within this class, it is instead used to ensure that this classes static counters are initalised for this pin and not for other pins.
     */
    template <typename Kernal, typename Pin>
    class Button2Threshold
    {
    protected:
        static inline std::size_t m_HighCounter = 0;
        static inline std::size_t m_LowCounter = 0;
    public:
        /**
         * @brief Resets the internal counters to zero.
         */
        static void Reset() noexcept
        {
            m_HighCounter = 0;
            m_LowCounter = 0;
        }
        /**
         * @brief Performs a single tick of the button counters.
         * 
         * @param input The pins value.
         * @return StateEvent The event that is raised, if no event is raised then the results is StateEvent::None.
         */
        static StateEvent Increment( bool input ) noexcept
        {
            StateEvent output = StateEvent::None;
            switch ( input )
            {
                case true:
                {
                    if (m_HighCounter == 0)
                    {
                        output = [&]
                        {
                            if constexpr (Kernal::LowCountEnabled)
                            {
                                if constexpr (Kernal::LongCountEnabled)
                                    if (m_LowCounter == Kernal::LongLowCount)
                                        return StateEvent::None;
                                //
                                if constexpr (Kernal::ShortCountEnabled)
                                    if (m_LowCounter >= Kernal::ShortLowCount)
                                        return StateEvent::ShortLow;
                            }
                            return StateEvent::None;
                        }();
                        m_LowCounter = 0;
                    }
                    m_HighCounter = std::min(m_HighCounter + 1, Kernal::MaxHighCount + 1);
                    if constexpr (Kernal::HighCountEnabled)
                    {
                        if constexpr (Kernal::LongCountEnabled)
                            if (m_HighCounter == Kernal::LongHighCount)
                                return StateEvent::LongHigh;
                    }
                }   return output;
                case false:
                {
                    if (m_LowCounter == 0)
                    {
                        output = [&]
                        {
                            if constexpr (Kernal::HighCountEnabled)
                            {
                                if constexpr (Kernal::LongCountEnabled)
                                    if (m_HighCounter == Kernal::LongHighCount)
                                        return StateEvent::None;
                                if constexpr (Kernal::ShortCountEnabled)
                                    if (m_HighCounter >= Kernal::ShortHighCount)
                                        return StateEvent::ShortHigh;
                            }
                            return StateEvent::None;
                        }();
                        m_HighCounter = 0;
                    }
                    m_LowCounter = std::min(m_LowCounter + 1, Kernal::MaxLowCount + 1);
                    if constexpr (Kernal::LowCountEnabled)
                    {
                        if constexpr (Kernal::LongCountEnabled)
                            if (m_LowCounter == Kernal::LongLowCount)
                                return StateEvent::LongLow;
                    }
                }   return output;
            };
            return StateEvent::None;
        }
    };
    /**
     * @brief A button instance.
     * 
     * Button instances are NOT unique as the hardware is not unique. 
     * The counters for any particular button type are shared. The counters are NOT shared between disimilar button types.
     * 
     * For example:
     * 
     * Button<K, P1, C> a;
     * Button<K, P1, F> b;
     * 
     * Both 'a' and 'b' share the kernal and the pin, this means they are querying the state of the same peripheral. They will share their counters.
     * 
     * However:
     * 
     * Button<K, P1, C> c;
     * Button<K, P2, F> d;
     * 
     * Now 'c' and 'd' do not share the same pin, this means they are querying DIFFERENT hardware and do not share the counters.
     * 
     * @tparam Kernal The kernal for the button.
     * @tparam Pin_t The pin which will be polled for buttons state.
     * @tparam Callback The callback, which is raised when a button event occurs.
     */
	template <typename Kernal, typename Pin_t, typename Callback>
	class Button2 : public Button2Threshold<Kernal, Pin_t>
	{
	public:
		using base_t     = Button2Threshold<Kernal, Pin_t>;
		using function_t = General::StaticLambdaWrapper<Callback, Pin_t>;
	private:
		function_t m_Event;
		Pin_t      m_Pin;
	public:
        /**
         * @brief Construct a new Button2 object
         * 
         * Note that any time a buttons constructor is called the internal counters are reset.
         * This means that if two instances of the same button (as per the kernal and the pin) they will both have their underlying 
         * counters reset.
         * 
         * @tparam C A forwarding reference to the callback.
         * @param event The callback function.
         */
        template <typename C>
		Button2( General::TypeList<Kernal, Pin_t>, C && event ) noexcept : 
		    m_Event { std::forward<C>( event ) },
		    m_Pin   { Mode::Input }
		{
            base_t::Reset();
        }
        /**
         * @brief Reads the state of the pin, converts it to bool (perhaps implicitly)
         * 
         * @return true The button state was converted to true.
         * @return false The buttons tate was converted to false.
         */
        bool Read() const noexcept
        {
            return m_Pin;
        }
        /**
         * @brief Runs a single tick on the button. This means the button counters are incremented.
         * 
         * If any event occurs the event callback will immediately be called.
         */
		void Update() noexcept
		{
            switch ( base_t::Increment( m_Pin ) )
            {
            case StateEvent::ShortLow:
                function_t::Run(StateEvent::ShortLow);
                return;
            case StateEvent::ShortHigh:
                function_t::Run(StateEvent::ShortHigh);
                return;
            case StateEvent::LongLow:
                function_t::Run(StateEvent::LongLow);
                return;
            case StateEvent::LongHigh:  
                function_t::Run(StateEvent::LongHigh);
                return;
            default:
                return;
            };
		}
	};
    /**
     * @brief The initialisation guide for the Button2 class.
     * 
     * @tparam K The kernal type.
     * @tparam P The pin type.
     * @tparam C The event callback type.
     */
	template <typename K, typename P, typename C>
	Button2(General::TypeList<K, P>, C)->Button2<K, P, C>;
    /**
     * @brief A kernal that expects a button press.
     */
    using SingleKernal2 = Button2Kernal<20, 0, 0, 0>;
    /**
     * @brief A kernal that expects a long button press.
     */
    using HoldKernal2 = Button2Kernal<0, 0, 40000, 0>;
    /**
     * @brief A kernal that expects a short press or a long button press.
     */
    using SingleHoldKernal2 = Button2Kernal<20, 0, 40000, 0>;
}