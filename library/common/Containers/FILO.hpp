#pragma once
#include <array>

namespace Containers
{
	/**
	 * @brief A first in last out data structure (also called a stack)
	 * 
	 * @tparam T The type of the elements.
	 * @tparam Length The capacity of the FILO.
	 */
	template <typename T, std::size_t Length>
	class FILO
	{
	protected:
		using this_t = FILO<T, Length>;
		//
		std::size_t           m_Endpoint = 0;
		std::array<T, Length> m_Buffer = {};
		//
		constexpr void DecrementEnd() noexcept
		{
			--m_Endpoint;
		}
		constexpr void IncrementEnd() noexcept
		{
			++m_Endpoint;
		}
		constexpr T& Next() noexcept
		{
			return m_Buffer[m_Endpoint];
		}
		constexpr size_t SinglePush(T const& data) noexcept
		{
			if (Full()) return 0;
			Next() = data;
			IncrementEnd();
			return 1;
		}
	public:
		using value_type 				= T;
		using iterator 					= value_type *;
		using reverse_iterator 			= std::reverse_iterator<iterator>;
		using const_iterator 			= const value_type *;
		using const_reverse_iterator 	= std::reverse_iterator<const_iterator>;
		/**
		 * @brief Construct a new FILO object.
		 * 
		 * This constructs an empty FILO object.
		 */
		constexpr FILO() = default;
		/**
		 * @brief Construct a fifo object with all members copy constructed from a parameter.
		 * 
		 * After copy construction the FILO will still be empty, this is just the default value of the members.
		 * 
		 * @param data The data to copy into all items of the FILO. 
		 */
		constexpr FILO(T const& data)
		{
			for (auto& item : m_Buffer)
				item = data;
		}
		/**
		 * @brief Increment the number of items in the FILO without copying in a new item.
		 * 
		 * @return true The push was successful.
		 * @return false The FILO is full.
		 */
		constexpr bool BlankPush() noexcept
		{
			if ( Full() )
			{
				return false;
			}
			else
			{
				IncrementEnd();
				return true;
			}
		}
		/**
		 * @brief Push a single item into the FILO.
		 * 
		 * @tparam Items The types of the items to push.
		 * @param items The items to push into the FILO.
		 * @return std::size_t The number of items pushed.
		 */
		template <typename... Items>
		constexpr std::size_t Push( Items const & ... items ) noexcept
		{
			return ( SinglePush( items ) + ... );
		}
		/**
		 * @brief Modify the stored length of the FILO. 
		 * 
		 * This can be used to undo a pop or to gain access to items beyond the current iterators.
		 * It can also be used to shrink the FIFO as a fast way to pop many items at once.
		 * 
		 * @param count The number of items that should be stored at the end of the resize.
		 * @return constexpr std::size_t The number of items that are now in the FILO (not nessarily count).
		 */
		constexpr std::size_t Resize(std::size_t count) noexcept
		{
			return (m_Endpoint = std::min(std::max(count, m_Endpoint), Length));
		}
		/**
		 * @brief Remove the back item from the FILO.
		 * 
		 * @return true The item was removed.
		 * @return false The FILO was already empty.
		 */
		constexpr bool Pop() noexcept
		{
			if (Empty()) return false;
			DecrementEnd();
			return true;
		}
		/**
		 * @brief Remove N items from the FILO, a fast way to pop multiple items.
		 * 
		 * @param n The number of items to pop.
		 * @return constexpr std::size_t The number of items that were popped (not necessarily n). zero could be returned, indicating the FILO was already empty.
		 */
		constexpr std::size_t Pop(std::size_t n) noexcept
		{
			n = std::min(n, Size());
			m_Endpoint -= n;
			return n;
		}
		/**
		 * @brief Checks whether a particular item exists in the FILO
		 * 
		 * @param data The data to compare with each item in the FILO.
		 * @return true The item exists.
		 * @return false The item doesn't exist.
		 */
		constexpr bool Exists(T const& data) const noexcept
		{
			for (std::size_t i = 0u; i < m_Endpoint; ++i)
				if (At(i) == data)
					return true;
			return false;
		}
		/**
		 * @brief Removes an item from the FILO and shifts the items to the correct place.
		 * 
		 * @param index The index to remove.
		 * @return true An item was removed.
		 * @return false An item was not removed.
		 */
		constexpr bool RemoveAt(std::size_t index) noexcept
		{
			if (Empty()) return false;

			if (index < m_Endpoint)
			{
				for (auto i = index; i < m_Endpoint - 1; ++i)
					std::swap(At(i), At(i + 1));
				return Pop();
			}
			return false;
		}
		/**
		 * @brief Clear all items from the FIFO.
		 * 
		 * NOTE: This does NOT trigger the destructors for items in the FIFO, they remain as they are.
		 * All this does is change the stored length to zero.
		 * 
		 * Destructors will be run if an item is attempted to be overridden
		 */
		constexpr void Clear() noexcept
		{
			m_Endpoint = 0;
		}
		/**
		 * @brief Return the first item in the FILO.
		 * 
		 * @return T& A reference to the first item in the FILO.
		 */
		constexpr T& Front() noexcept
		{
			return m_Buffer[0];
		}
		/**
		 * @brief Return the first item in the FILO.
		 * 
		 * @return T const & A const reference to the first item in the FILO.
		 */
		constexpr T const& Front() const noexcept
		{
			return m_Buffer[0];
		}
		/**
		 * @brief Get a reference to the last item in the FILO.
		 * 
		 * @return T& A reference to the llast item in the FILO.
		 */
		constexpr T& Back() noexcept
		{
			return m_Buffer[(m_Endpoint + Length - 1u) % Length];
		}
		/**
		 * @brief Get a const reference to the last item in the FILO.
		 * 
		 * @return T const & A const reference to the last item in the FILO.
		 */
		constexpr T const& Back() const noexcept
		{
			return m_Buffer[(m_Endpoint + Length - 1u) % Length];
		}
		/**
		 * @brief Get the number of items stored in the FILo
		 * 
		 * @return std::size_t The number of items stored in the FILo.
		 */
		constexpr std::size_t Size() const noexcept
		{
			return m_Endpoint;
		}
		/**
		 * @brief Get the maximum number of items that can be stored in the FILO.
		 * 
		 * @return std::size_t The capacity of the FILO.
		 */
		constexpr std::size_t Capacity() const noexcept
		{
			return Length;
		}
		/**
		 * @brief Get the remaining space in the FILO.
		 * 
		 * @return std::size_t The remaining space in the FILO.
		 */
		constexpr std::size_t Space() const noexcept
		{
			return Capacity() - Size();
		}
		/**
		 * @brief Check whether the FILO is empty.
		 * 
		 * @return true The filo is empty.
		 * @return false The FILO is not empty.
		 */
		constexpr bool Empty() const noexcept
		{
			return Size() == 0;
		}
		/**
		 * @brief Check whether the FILO is full.
		 * 
		 * @return true The FILO is full.
		 * @return false The FILO is not full.
		 */
		constexpr bool Full() const noexcept
		{
			return (Size() == Length);
		}
		/**
		 * @brief Get a pointer to the first item in the FILO.
		 * 
		 * The data in the FILO is contigious.
		 * 
		 * @return T * A pointer to the first item in the FILO.
		 */
		constexpr decltype(auto) Data() noexcept
		{
			return m_Buffer.data();
		}
		/**
		 * @brief Get a const pointer to the first item in the FILO.
		 * 
		 * The data in the FILO is contigious.
		 * 
		 * @return decltype(auto) A pointer to the first item in the FILO.
		 */
		constexpr decltype(auto) Data() const noexcept
		{
			return m_Buffer.data();
		}
		/**
		 * @brief Get the begin iterator.
		 * 
		 * @return iterator The begin iterator.
		 */
		constexpr iterator begin() noexcept
		{
			return m_Buffer.data();
		}
		/**
		 * @brief Get the end iterator.
		 * 
		 * @return iterator The end iterator. 
		 */
		constexpr iterator end() noexcept
		{
			return m_Buffer.data() + Size();
		}
		/**
		 * @brief Get the begin iterator.
		 * 
		 * @return iterator The begin iterator.
		 */
		constexpr const_iterator begin() const noexcept
		{
			return m_Buffer.data();
		}
		/**
		 * @brief Get the end iterator.
		 * 
		 * @return iterator The end iterator. 
		 */
		constexpr const_iterator end() const noexcept
		{
			return m_Buffer.data() + Size();
		}
		/**
		 * @brief Get the reversed begin iterator.
		 * 
		 * @return iterator The reversed begin iterator. 
		 */
		constexpr reverse_iterator rbegin() noexcept
		{
			return reverse_iterator{ end() };
		}
		/**
		 * @brief Get the reversed end iterator.
		 * 
		 * @return iterator The reversed end iterator. 
		 */
		constexpr reverse_iterator rend() noexcept
		{
			return reverse_iterator{ begin() };
		}
		/**
		 * @brief Get the const reversed begin iterator.
		 * 
		 * @return iterator The const reversed begin iterator. 
		 */
		constexpr const_reverse_iterator rbegin() const noexcept
		{
			return const_reverse_iterator{ end() };
		}
		/**
		 * @brief Get the const reversed end iterator.
		 * 
		 * @return iterator The const reversed end iterator. 
		 */
		constexpr const_reverse_iterator rend() const noexcept
		{
			return const_reverse_iterator{ begin() };
		}
		/**
		 * @brief A pointer to the start of the data.
		 * 
		 * @return constexpr decltype(auto) 
		 */
		constexpr decltype(auto) cbegin() const noexcept
		{
			return m_Buffer.data();
		}
		/**
		 * @brief A pointer to the end of the data.
		 * 
		 * @return constexpr decltype(auto) 
		 */
		constexpr decltype(auto) cend() const noexcept
		{
			return m_Buffer.data() + Size();
		}
		/**
		 * @brief Get the item at an index.
		 * 
		 * @param index 
		 * @return T& 
		 */
		constexpr T& At(std::size_t index) noexcept
		{
			return m_Buffer.at(index);
		}
		/**
		 * @brief Get the item at the index.
		 * 
		 * @param index The index of the item.
		 * @return T const& A const reference to the item.
		 */
		constexpr T const& At(std::size_t index) const noexcept
		{
			return m_Buffer.at(index);
		}
		/**
		 * @brief Get the item at the index.
		 * 
		 * @param index The index to get the item.
		 * @return T& A reference to the item at the requested index.
		 */
		constexpr T& operator[](std::size_t index) noexcept
		{
			return m_Buffer[index];
		}
		/**
		 * @brief Get the item at the index.
		 * 
		 * @param index The index to get the item.
		 * @return T const & A reference to the item at the requested index.
		 */
		constexpr T const& operator[](std::size_t index) const noexcept
		{
			return m_Buffer[index];
		}
	};
}