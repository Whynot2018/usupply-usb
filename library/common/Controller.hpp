#pragma once

#include <cstdint>

namespace General
{
	/**
	 * @brief The type of controller to use.
	 */
	enum class ControllerType : std::uint8_t
	{
		P   = 0b100,
		I   = 0b010,
		D   = 0b001,
		PID = P | I | D,
		PI  = P | I,
		PD  = P | D,
		ID  = I | D
	};
	/**
	 * @brief A PID controller class.
	 * 
	 * @tparam T The variable type that is used for controlling.
	 */
	template <typename T>
	class Controller
	{
	private:
		T const m_Kp, m_Ki, m_Kd;
		T       m_I;
		T const m_ILimit;
		//
		// Always part of the system
		T m_Error, m_Set;
	public:
		/**
		 * @brief Construct a new Controller object
		 * 
		 * @param k_p The proportional component of the controller.
		 * @param k_i The integral component of the controller.
		 * @param k_d The derivative component of the controller.
		 * @param sample_time The sample time.
		 * @param windup_limit The limit to the integral component of the controller.
		 */
		Controller( T const& k_p, T const& k_i, T const& k_d, T const& sample_time, T const& windup_limit ) noexcept:
		    m_Kp( k_p ),
		    m_Ki( k_i * sample_time ),
		    m_Kd( k_d / sample_time ),
		    m_I( 0 ),
		    m_ILimit( windup_limit ),
		    m_Error( 0 ),
		    m_Set( 0 )
		{}
		/**
		 * @brief Gets the classes setpoint as a mutable reference.
		 * 
		 * @return auto& A reference to the setpoint.
		 */
		T& Setpoint() noexcept
		{
			return m_Set;
		}
		/**
		 * @brief Update a single tick of the controller.
		 * 
		 * @param feedback The feedback value.
		 * @return T The resulting control effort.
		 */
		T Update( T const& feedback ) noexcept
		{
			auto const error   = m_Set - feedback;
			auto const d_error = ( m_Error - error );
			m_Error            = error;
			auto const P       = m_Kp * error;
			m_I += m_Ki * error;
			m_I          = std::clamp( m_I, -m_ILimit, m_ILimit );
			auto const D = m_Kd * d_error;
			return ( P + m_I + D );
		}
		/**
		 * @brief Clears the state of the PID controller.
		 * 
		 * This clears the integral component and the current error.
		 */
		void Clear() noexcept
		{
			m_Error = 0;
			m_I     = 0;
		}
	};
}