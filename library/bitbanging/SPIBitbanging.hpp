#pragma once

#include "Integer.hpp"
#include "Pin.hpp"
#include <array>

namespace IO::SPI
{
	enum class Polarity : bool
	{
		NonInverted = false,
		Inverted    = true,
	};
	enum class Phase : bool
	{
		TrailingEdge = false,
		LeadingEdge  = true,
	};

	template <Polarity CPOL, Phase CPHA, typename SPI_CS, typename SPI_SCLK, typename SPI_MOSI, typename SPI_MISO, unsigned DELAY = 0>
	class Bitbang
	{
	private:
		SPI_CS   m_CS;
		SPI_SCLK m_SCLK;
		SPI_MOSI m_MOSI;
		SPI_MISO m_MISO;

		static void Delay()
		{
			if constexpr (DELAY)
			{
				for(volatile unsigned i = 0; i < DELAY; ++i);
			}
		}

		static IO::State PolarityState()
		{
			return (State)CPOL;
		}
		inline void Select()
		{
			m_CS = IO::State::Low;
			Delay();
		}
		void Deselect()
		{
			m_MOSI = IO::State::High;
			Delay();
			m_CS   = IO::State::High;
			Delay();
		}
		void ToggleCLK()
		{
			m_SCLK.Toggle();
			Delay();
		}
		void DataOut( bool const pInput )
		{
			m_MOSI = pInput;
			Delay();
		}
		bool DataIn()
		{
			return m_MISO;
		}

		template <unsigned bits>
		static bool MSB_Shift_Out( General::UnsignedInt_t<bits>& pInput )
		{
			auto const max      = General::UnsignedInt<bits>::max;
			auto const ValueMSB = ( 1 << ( bits - 1 ) );
			auto const output   = ( pInput & max ) >= ValueMSB;
			pInput <<= 1;
			pInput &= max;
			return output;
		}
		template <unsigned bits>
		static void MSB_Shift_In( bool const pInput, General::UnsignedInt_t<bits>& pBuffer )
		{
			pBuffer >>= 1;
			pBuffer |= pInput << ( bits - 1 );
		}

		bool WriteTick( bool const pOutbit )
		{
			if constexpr ( CPHA == Phase::TrailingEdge )
			{
				DataOut( pOutbit );
				ToggleCLK();
				auto const output = DataIn();
				ToggleCLK();
				return output;
			}
			if constexpr ( CPHA == Phase::LeadingEdge )
			{
				ToggleCLK();
				DataOut( pOutbit );
				ToggleCLK();
				return DataIn();
			}
			return false;
		}

	public:
		constexpr Bitbang() :
		    m_CS	( IO::Mode::Output, IO::State::High ),
		    m_SCLK	( IO::Mode::Output, PolarityState() ),
		    m_MOSI	( IO::Mode::Output, IO::State::High ),
		    m_MISO	( IO::Mode::Input ) 
		{}

		//Manual write/read commands
		inline void WriteStart()
		{
			Select();
		}
		template <unsigned bits = 8u>
		General::UnsignedInt_t<bits> WriteData( General::UnsignedInt_t<bits> pInput )
		{
			General::UnsignedInt_t<bits> read = 0u;
			for ( auto i = 0u; i < bits; ++i )
			{
				auto const OutMSB = MSB_Shift_Out<bits>( pInput );
				auto const InMSB  = WriteTick( OutMSB );
				MSB_Shift_In<bits>( InMSB, read );
			}
			return read;
		}
		inline void WriteEnd()
		{
			Deselect();
		}

		//Packaged write command
		template <unsigned bits = 8u>
		General::UnsignedInt_t<bits> Write( General::UnsignedInt_t<bits> pInput )
		{
			WriteStart();
			auto const read = WriteData<bits>( pInput );
			WriteEnd();
			return read;
		}

		//Packaged read command
		template <unsigned bits = 8u>
		General::UnsignedInt_t<bits> Read()
		{
			return Write<bits>( (General::UnsignedInt_t<bits>)0 );
		}
	};
}