#pragma once

#include "stm32f0xx.h"
#include "EventDispatch.hpp"
#include "InterruptRegisters.hpp"
#include "cortexm/ExceptionHandlers.h"
#include "Macros.hpp"
#include "Meta.hpp"
#include "Math.hpp"

namespace System
{
	enum class InterruptSource
	{
#ifdef STM32F070x6
		//Cortex M0 Interrupts
		eNonMaskableInt 			= NonMaskableInt_IRQn,       
		eHardFault 					= HardFault_IRQn,             
		eSVC 						= SVC_IRQn,                   
		ePendSV 					= PendSV_IRQn,                
		eSysTick 					= SysTick_IRQn,              

		//System interrupts
		eWWDG                       = WWDG_IRQn,
		eRTC                        = RTC_IRQn,
		eFLASH                      = FLASH_IRQn,
		eRCC_CRS                    = RCC_CRS_IRQn,
		eEXTI0_1                    = EXTI0_1_IRQn,
		eEXTI2_3                    = EXTI2_3_IRQn,
		eEXTI4_15                   = EXTI4_15_IRQn,
		eDMA_CH1 					= DMA1_Channel1_IRQn,
		eDMA_CH2_3                  = DMA1_Channel2_3_IRQn,
		eDMA2_CH1_2 				= eDMA_CH2_3,
		eDMA_CH4_5_6_7              = DMA1_Channel4_5_6_7_IRQn,
		eDMA2_CH3_4_5  				= eDMA_CH4_5_6_7,
		eADC_COMP                   = ADC1_COMP_IRQn,
		eTIM1_BRK_UP_TRG_COM        = TIM1_BRK_UP_TRG_COM_IRQn,
		eTIM1_CC                    = TIM1_CC_IRQn,
		eTIM3						= TIM3_IRQn,
		eTIM14                      = TIM14_IRQn,
		eTIM16                      = TIM16_IRQn,
		eTIM17                      = TIM17_IRQn,
		eI2C1                       = I2C1_IRQn,
		eSPI1                       = SPI1_IRQn,
		eUSART1                     = USART1_IRQn,
		eUSART2                     = USART2_IRQn,
		eUSB                        = USB_IRQn,
#endif
#ifdef STM32F072
		//Cortex M0 Interrupts
		eNonMaskableInt 			= NonMaskableInt_IRQn,       
		eHardFault 					= HardFault_IRQn,             
		eSVC 						= SVC_IRQn,                   
		ePendSV 					= PendSV_IRQn,                
		eSysTick 					= SysTick_IRQn,              

		//System interrupts
		eWWDG                       = WWDG_IRQn,
		ePVD_VDDIO2                 = PVD_VDDIO2_IRQn,
		eRTC                        = RTC_IRQn,
		eFLASH                      = FLASH_IRQn,
		eRCC_CRS                    = RCC_CRS_IRQn,
		eEXTI0_1                    = EXTI0_1_IRQn,
		eEXTI2_3                    = EXTI2_3_IRQn,
		eEXTI4_15                   = EXTI4_15_IRQn,
		eTSC                        = TSC_IRQn,
		eDMA_CH1                    = DMA1_Channel1_IRQn,
		eDMA_CH2_3                  = DMA1_Channel2_3_IRQn,
		eDMA2_CH1_2 				= eDMA_CH2_3,
		eDMA_CH4_5_6_7              = DMA1_Channel4_5_6_7_IRQn,
		eDMA2_CH3_4_5  				= eDMA_CH4_5_6_7,
		eADC_COMP                   = ADC1_COMP_IRQn,
		eTIM1_BRK_UP_TRG_COM        = TIM1_BRK_UP_TRG_COM_IRQn,
		eTIM1_CC                    = TIM1_CC_IRQn,
		eTIM2                       = TIM2_IRQn,
		eTIM3                       = TIM3_IRQn,
		eTIM6_DAC                   = TIM6_DAC_IRQn,
		eTIM7                       = TIM7_IRQn,
		eTIM14                      = TIM14_IRQn,
		eTIM15                      = TIM15_IRQn,
		eTIM16                      = TIM16_IRQn,
		eTIM17                      = TIM17_IRQn,
		eI2C1                       = I2C1_IRQn,
		eI2C2                       = I2C2_IRQn,
		eSPI1                       = SPI1_IRQn,
		eSPI2                       = SPI2_IRQn,
		eUSART1                     = USART1_IRQn,
		eUSART2                     = USART2_IRQn,
		eUSART3_4                   = USART3_4_IRQn,
		eCEC_CAN                    = CEC_CAN_IRQn,
		eUSB                        = USB_IRQn,
#endif
	};
	/**
	 * @brief Setts the priority of the interrupt source.
	 * 
	 * @tparam source The interrupt source to set the priority.
	 * @return true Priorty was set.
	 * @return false Priority was not set.
	 */
	template <InterruptSource source>
	constexpr bool SettablePriority() noexcept
	{
		switch(source)
		{
		case InterruptSource::eNonMaskableInt:
		case InterruptSource::eHardFault:
			return false;
		default: 
			return true;
		}
	}
	/**
	 * @brief Get the interrupt source for an external interrupt.
	 * 
	 * @tparam pin The pin to correlate with an interrupt source.
	 * @return InterruptSource The interrupt source.
	 */
	template <unsigned pin>
	constexpr InterruptSource ExternalInterrupt() noexcept
	{
		#ifdef STM32F070x6
		if ( General::Between( pin, 0, 1	) ) return InterruptSource::eEXTI0_1;
		if ( General::Between( pin, 2, 3	) ) return InterruptSource::eEXTI2_3;
		if ( General::Between( pin, 4, 15 	) ) return InterruptSource::eEXTI4_15;
		#endif
	}
	/**
	 * @brief Whether the source can be enabled.
	 * 
	 * @tparam source The source of the interrupt source.
	 * @return true The interrupt can be enabled.
	 * @return false The interrupt can not be enabled.
	 */
	template <InterruptSource source>
	constexpr bool Enablable() noexcept
	{
		return (General::UnderlyingValue(source) >= 0);
	}
	/**
	 * @brief The underlying implementation of the interrupt queue.
	 * 
	 * @tparam source The source of the interrupt.
	 * @tparam buffer_size The size of the buffer.
	 */
	template <InterruptSource source, unsigned buffer_size = 2u>
	using InterruptDispatch = General::EventDispatch<typename std::integral_constant<InterruptSource, source>::type, buffer_size>;
	/**
	 * @brief A manager class for an interrupt.
	 * 
	 * @tparam T The key type to insure underlying queues are unique.
	 * @tparam source The source of the interrupt.
	 * @tparam priority The priority of the interrupt.
	 */
	template <typename T, InterruptSource source, unsigned priority = 2>
	struct Interrupt : InterruptDispatch<source>
	{
		using void_function_t = void(*)();
		using dispatcher_t = InterruptDispatch<source>;
		/**
		 * @brief Initialises the interrupt and enables it in the NVIC
		 * it does not enable peripheral specific flags for interrupts.
		 * 
		 * @param callback The function to run when interrupt is triggered.
		 */
		ALWAYS_INLINE Interrupt( void_function_t callback ) noexcept
		{
			if ( dispatcher_t::Empty() )
			{
				/**
				 * Set USB interrupt priority
				 */
				if constexpr ( SettablePriority<source>() )
				{
					NVIC_SetPriority( (IRQn)source, priority );
				}
				/**
				 * Enable USB interrupt
				 */
				if constexpr ( Enablable<source>() )
				{
					NVIC_EnableIRQ( (IRQn)source );
				}
			}
			dispatcher_t::Push( callback );
		}
		ALWAYS_INLINE Interrupt() noexcept : Interrupt{ &T::Interrupt } {}
		/**
		 * @brief Disables the interrupt if nothing is using it.
		 */
		ALWAYS_INLINE ~Interrupt()
		{
			dispatcher_t::Pop();
			if ( dispatcher_t::Empty() )
			{
				/**
				 * Disable USB interrupt
				 */
				if constexpr ( Enablable<source>() )
				{
					NVIC_DisableIRQ( USB_IRQn );
				}
			}
		}
	};
}

template <System::InterruptSource source>
using InterruptDispatch = System::InterruptDispatch<source>;