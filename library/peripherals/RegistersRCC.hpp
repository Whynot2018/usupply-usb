#pragma once

#include "Meta.hpp"
#include "Pin.hpp"
#include "Math.hpp"
#include "Register.hpp"
#include "stm32f0xx.h"
#include <cstdint>

namespace Peripherals::RCCGeneral
{
	enum class Clocks : std::uint32_t
	{
		LSE,
		LSI,
		HSE,
		HSI,
		HSI_2,
		HSI14,
		HSI48
	};
	enum class RTCClockSource : std::uint32_t
	{
		None	=	0b00,
		LSE		=	0b01,
		LSI		=	0b10,
		HSE_32	=	0b11
	};
	enum class SystemClockSource : std::uint32_t
	{
		HSI 	= 	0b00,
		HSE 	= 	0b01,
		PLL 	= 	0b10,
		HSI48 	= 	0b11
	};
	enum class ClockOutput : std::uint32_t
	{
		Disabled	= 0b0000,
		RC14MHz		= 0b0001,
		LSI			= 0b0010,
		LSE			= 0b0011,
		SYSCLK		= 0b0100,
		HSE			= 0b0110,
		PLL			= 0b0111,
		HSI8		= 0b0101,
		HSI48		= 0b1000,
		PLL_2		= 0b1001,
	};
	enum class ClockState : bool
	{
		On 	= true,
		Off = false
	};
	enum class ClockOutputDivision : std::uint32_t
	{
		Div1	= 0,
		Div2	= 1,
		Div4	= 2,
		Div8	= 3,
		Div16	= 4,
		Div32	= 5,
		Div64	= 6,
		Div128	= 7
	};
	enum class PLLMultiply : std::uint32_t
	{
		x2 	=	0,
		x3 	=	1,
		x4 	=	2,
		x5 	=	3,
		x6 	=	4,
		x7 	=	5,
		x8 	=	6,
		x9 	=	7,
		x10	=	8,
		x11	=	9,
		x12	=	10,
		x13	=	11,
		x14	=	12,
		x15	=	13,
		x16	=	14
	};
	enum class PLLDivision : std::uint32_t
	{
		Div1	=	0,
		Div2	=	1,
		Div3	=	2,
		Div4	=	3,
		Div5	=	4,
		Div6	=	5,
		Div7	=	6,
		Div8	=	7,
		Div9	=	8,
		Div10	=	9,
		Div11	=	10,
		Div12	=	11,
		Div13	=	12,
		Div14	=	13,
		Div15	=	14,
		Div16	=	15,
	};
	enum class PLLSource : std::uint32_t
	{
#ifdef STM32F072
		HSI_2			= 0b00,
		HSI_PREDIV		= 0b01,
		HSE_PREDIV		= 0b10,
		HSI48_PREDIV	= 0b11
#endif
#ifdef STM32F070x6
		HSI_2			= 0b0,
		HSE_PREDIV		= 0b1,
#endif
	};
	enum class PCLKDivision : std::uint32_t
	{
		HCLK_1	= 0b000,
		HCLK_2	= 0b100,
		HCLK_4	= 0b101,
		HCLK_8	= 0b110,
		HCLK_16	= 0b111
	};
	enum class HCLKDivision : std::uint32_t
	{
		SYSCLK_1	=	0b0000,
		SYSCLK_2	=	0b1000,
		SYSCLK_4	=	0b1001,
		SYSCLK_8	=	0b1010,
		SYSCLK_16	=	0b1011,
		SYSCLK_64	=	0b1100,
		SYSCLK_128	=	0b1101,
		SYSCLK_256	=	0b1110,
		SYSCLK_512	=	0b1111,
	};
	enum class DriveCapability : std::uint32_t
	{
		Lowest	=	0b00,
		Low		=	0b10,
		High	=	0b01,
		Highest	=	0b11
	};
	enum class USARTClock : std::uint32_t
	{
		PCLK 	= 0b00,
		SYSCLK 	= 0b01,
		LSE 	= 0b10,
		HSI 	= 0b11
	};
	enum class USBClock : std::uint32_t
	{
		HSI48 = 0,
		LSE = 1
	};
	enum class HDMIClock : std::uint32_t
	{
		HSI_244 = 0,
		LSE = 1
	};
	enum class I2CClock : std::uint32_t
	{
		HSI = 0,
		SYSCLK = 1
	};

	template <std::size_t A>
	struct CR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		//PLL register bits
		auto PLLRDY () { return base_t::template Actual<RCC_CR_PLLRDY>(); }
		auto PLLON  () { return base_t::template Actual<RCC_CR_PLLON>(); }

		//PLL Management functions
		void EnablePLL()
		{
			PLLON() = true;
			while ( !PLLRDY() );
		}
		void DisablePLL()
		{
			PLLON() = false;
			while ( PLLRDY() );
		}

		//Enable clock security
		auto CSSON  () { return base_t::template Actual<RCC_CR_CSSON>(); }

		//High speed external oscillator bits
		auto HSEBYP () { return base_t::template Actual<RCC_CR_HSEBYP>(); }
		auto HSERDY () { return base_t::template Actual<RCC_CR_HSERDY>(); }
		auto HSEON  () { return base_t::template Actual<RCC_CR_HSEON>(); }

		//HSE Management functions
		void EnableHSE()
		{
			HSEON() = true;		//Enable the clock
			while(!HSERDY());	//Wait for it to stable
		}
		void DisableHSE()
		{
			HSEON() = false;	//Disable the clock
			while(HSERDY());	//Wait for it to disable
		}
		void ConnectHSE()
		{
			HSEBYP() = false;	//Connect it to system
		}
		void BypassHSE()
		{
			HSEBYP() = true;	//Disconnect it to system
		}

		//High speed internal oscillator bits
		auto HSICAL () { return base_t::template Actual<RCC_CR_HSICAL>(); }
		auto HSITRIM() { return base_t::template Actual<RCC_CR_HSITRIM>(); }
		auto HSIRDY () { return base_t::template Actual<RCC_CR_HSIRDY>(); }
		auto HSION  () { return base_t::template Actual<RCC_CR_HSION>(); }

		//HSI Management functions, No calibration provided
		// these chips are factory calibrated
		void EnableHSI()
		{
			HSION() = true;
			while (!HSIRDY());
		}
		void DisableHSI()
		{
			HSION() = false;
			while (HSIRDY());
		}
	};

	template <std::size_t A>
	struct CFGR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		//PLL Bits
		auto PLLMUL  () { return base_t::template Actual<RCC_CFGR_PLLMUL>();}
		auto PLLSRC  () { return base_t::template Actual<RCC_CFGR_PLLSRC_1>();}

		//PLL Managment
		void SetPLLMultiply(PLLMultiply const pMultiply)
		{
			PLLMUL() = (std::uint32_t)pMultiply;
		}
		void SetPLLSource(PLLSource const pInput)
		{
			PLLSRC() = (std::uint32_t)pInput;
		}

		//Microcontroller Clock Output bits
		auto PLLNODIV() { return base_t::template Actual<RCC_CFGR_PLLNODIV>();}
		auto MCOPRE  () { return base_t::template Actual<RCC_CFGR_MCO_PRE>();}
		auto MCO     () { return base_t::template Actual<RCC_CFGR_MCO>();}

		//Microcontroller Clock Output Managment functions
		void SetupOutput(ClockOutput pOutput, ClockOutputDivision const pDivision)
		{
			// Correct for the PLL_2 output (not a real register setting)
			if (pOutput == ClockOutput::PLL_2)
			{
				pOutput = ClockOutput::PLL;
				PLLNODIV() = false;
			}
			else PLLNODIV() = true;

			//
			MCO() 		= (std::uint32_t)ClockOutput::Disabled;
			MCOPRE() 	= (std::uint32_t)pDivision;
			MCO() 		= (std::uint32_t)pOutput;
		}

		//Clock Prescaler registers
		auto PPRE    () { return base_t::template Actual<RCC_CFGR_PPRE>();}
		auto HPRE    () { return base_t::template Actual<RCC_CFGR_HPRE>();}

		//Prescaler setup function
		void SetupPrescalers(HCLKDivision pHCLKPrescaler, PCLKDivision pPCLKPrescaler)
		{
			HPRE() = static_cast<std::uint32_t>(pHCLKPrescaler);
			PPRE() = static_cast<std::uint32_t>(pPCLKPrescaler);
		}

		//Clock selection registers
		auto SW      () { return base_t::template Actual<RCC_CFGR_SW>();}
		auto SWS     () { return base_t::template Actual<RCC_CFGR_SWS>();}

		//Clock selection Management functions
		void SelectSystemClock(SystemClockSource const pSource)
		{
			auto const source = (std::uint32_t)pSource;
			SW() = source;
			while(SWS() != source);
		}

		//This bit is obselete, therefore not enabling it.
		//auto ADCPRE  () { return Actual<RCC_CFGR_ADCPRE>();}

		//This register is redundant and is a copy of what appears
		// in RCC_CFGR2 PREDIV bit 0.
		//auto PLLXTPRE() { return Actual<RCC_CFGR_PLLXTPRE>();}
	};

	template <std::size_t A>
	struct CIR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto CSSC      () { return base_t::template Actual<RCC_CIR_CSSC>(); 		 }
		auto HSI48RDYC () { return base_t::template Actual<RCC_CIR_HSI48RDYC>();  }
		auto HSI14RDYC () { return base_t::template Actual<RCC_CIR_HSI14RDYC>();  }
		auto PLLRDYC   () { return base_t::template Actual<RCC_CIR_PLLRDYC>(); 	 }
		auto HSERDYC   () { return base_t::template Actual<RCC_CIR_HSERDYC>(); 	 }
		auto HSIRDYC   () { return base_t::template Actual<RCC_CIR_HSIRDYC>(); 	 }
		auto LSERDYC   () { return base_t::template Actual<RCC_CIR_LSERDYC>(); 	 }
		auto LSIRDYC   () { return base_t::template Actual<RCC_CIR_LSIRDYC>(); 	 }
		auto HSI48RDYIE() { return base_t::template Actual<RCC_CIR_HSI48RDYIE>(); }
		auto HSI14RDYIE() { return base_t::template Actual<RCC_CIR_HSI14RDYIE>(); }
		auto PLLRDYIE  () { return base_t::template Actual<RCC_CIR_PLLRDYIE>(); 	 }
		auto HSERDYIE  () { return base_t::template Actual<RCC_CIR_HSERDYIE>(); 	 }
		auto HSIRDYIE  () { return base_t::template Actual<RCC_CIR_HSIRDYIE>(); 	 }
		auto LSERDYIE  () { return base_t::template Actual<RCC_CIR_LSERDYIE>(); 	 }
		auto LSIRDYIE  () { return base_t::template Actual<RCC_CIR_LSIRDYIE>(); 	 }
		auto CSSF      () { return base_t::template Actual<RCC_CIR_CSSF>(); 		 }
		auto HSI48RDYF () { return base_t::template Actual<RCC_CIR_HSI48RDYF>();  }
		auto HSI14RDYF () { return base_t::template Actual<RCC_CIR_HSI14RDYF>();  }
		auto PLLRDYF   () { return base_t::template Actual<RCC_CIR_PLLRDYF>(); 	 }
		auto HSERDYF   () { return base_t::template Actual<RCC_CIR_HSERDYF>(); 	 }
		auto HSIRDYF   () { return base_t::template Actual<RCC_CIR_HSIRDYF>(); 	 }
		auto LSERDYF   () { return base_t::template Actual<RCC_CIR_LSERDYF>(); 	 }
		auto LSIRDYF   () { return base_t::template Actual<RCC_CIR_LSIRDYF>(); 	 }
	};

	template <std::size_t A>
	struct AHBENR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto TSCEN    () { return base_t::template Actual<RCC_AHBENR_TSCEN>(); 	}
		auto GPIOFEN  () { return base_t::template Actual<RCC_AHBENR_GPIOFEN>(); }
		auto GPIOEEN  () { return base_t::template Actual<RCC_AHBENR_GPIOEEN>(); }
		auto GPIODEN  () { return base_t::template Actual<RCC_AHBENR_GPIODEN>(); }
		auto GPIOCEN  () { return base_t::template Actual<RCC_AHBENR_GPIOCEN>(); }
		auto GPIOBEN  () { return base_t::template Actual<RCC_AHBENR_GPIOBEN>(); }
		auto GPIOAEN  () { return base_t::template Actual<RCC_AHBENR_GPIOAEN>(); }
		auto CRCEN    () { return base_t::template Actual<RCC_AHBENR_CRCEN>(); 	}
		auto FLITFEN  () { return base_t::template Actual<RCC_AHBENR_FLITFEN>();}
		auto SRAMEN   () { return base_t::template Actual<RCC_AHBENR_SRAMEN>(); }
		auto DMA2EN   () { return base_t::template Actual<RCC_AHBENR_DMA2EN>(); }
		auto DMAEN    () { return base_t::template Actual<RCC_AHBENR_DMAEN>(); 	}
	};

	template <std::size_t A>
	struct APB2ENR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto DBGMCUEN    () { return base_t::template Actual<RCC_APB2ENR_DBGMCUEN>(); 	}
		auto TIM17EN     () { return base_t::template Actual<RCC_APB2ENR_TIM17EN>(); 	}
		auto TIM16EN     () { return base_t::template Actual<RCC_APB2ENR_TIM16EN>(); 	}
		auto TIM15EN     () { return base_t::template Actual<RCC_APB2ENR_TIM15EN>(); 	}
		auto USART1EN    () { return base_t::template Actual<RCC_APB2ENR_USART1EN>(); 	}
		auto SPI1EN      () { return base_t::template Actual<RCC_APB2ENR_SPI1EN>(); 	}
		auto TIM1EN      () { return base_t::template Actual<RCC_APB2ENR_TIM1EN>(); 	}
		auto ADCEN       () { return base_t::template Actual<RCC_APB2ENR_ADCEN>(); 		}
		auto USART8EN    () { return base_t::template Actual<RCC_APB2ENR_USART8EN>(); 	}
		auto USART7EN    () { return base_t::template Actual<RCC_APB2ENR_USART7EN>(); 	}
		auto USART6EN    () { return base_t::template Actual<RCC_APB2ENR_USART6EN>(); 	}
		auto SYSCFGCOMPEN() { return base_t::template Actual<RCC_APB2ENR_SYSCFGCOMPEN>();}
	};

	template <std::size_t A>
	struct APB1ENR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto CECEN   () { return base_t::template Actual<RCC_APB1ENR_CECEN>(); 	}
		auto DACEN   () { return base_t::template Actual<RCC_APB1ENR_DACEN>(); 	}
		auto PWREN   () { return base_t::template Actual<RCC_APB1ENR_PWREN>(); 	}
		auto CRSEN   () { return base_t::template Actual<RCC_APB1ENR_CRSEN>(); 	}
		auto CANEN   () { return base_t::template Actual<RCC_APB1ENR_CANEN>(); 	}
		auto USBEN   () { return base_t::template Actual<RCC_APB1ENR_USBEN>(); 	}
		auto I2C2EN  () { return base_t::template Actual<RCC_APB1ENR_I2C2EN>(); 	}
		auto I2C1EN  () { return base_t::template Actual<RCC_APB1ENR_I2C1EN>(); 	}
		auto USART5EN() { return base_t::template Actual<RCC_APB1ENR_USART5EN>();}
		auto USART4EN() { return base_t::template Actual<RCC_APB1ENR_USART4EN>();}
		auto USART3EN() { return base_t::template Actual<RCC_APB1ENR_USART3EN>();}
		auto USART2EN() { return base_t::template Actual<RCC_APB1ENR_USART2EN>();}
		auto SPI2EN  () { return base_t::template Actual<RCC_APB1ENR_SPI2EN>(); 	}
		auto WWDGEN  () { return base_t::template Actual<RCC_APB1ENR_WWDGEN>(); 	}
		auto TIM14EN () { return base_t::template Actual<RCC_APB1ENR_TIM14EN>(); }
		auto TIM7EN  () { return base_t::template Actual<RCC_APB1ENR_TIM7EN>(); 	}
		auto TIM6EN  () { return base_t::template Actual<RCC_APB1ENR_TIM6EN>(); 	}
		auto TIM3EN  () { return base_t::template Actual<RCC_APB1ENR_TIM3EN>(); 	}
		auto TIM2EN  () { return base_t::template Actual<RCC_APB1ENR_TIM2EN>(); 	}
	};

	template <std::size_t A>
	struct BDCR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto BDRST () { return base_t::template Actual<RCC_BDCR_BDRST>();     	}
		auto RTCEN () { return base_t::template Actual<RCC_BDCR_RTCEN>();     	}
		auto RTCSEL() { return base_t::template Actual<RCC_BDCR_RTCSEL>();    	}

		//SetupRTC : Requires selection of RTC domain
		void ResetRTC()
		{
			BDRST() = true;
		}
		void EnableRTC(RTCClockSource const pSource = RTCClockSource::LSE)
		{
			RTCSEL() = (std::uint32_t)pSource;
			RTCEN() = true;
		}

		//Low Speed External Oscillor Registers
		auto LSEDRV() { return base_t::template Actual<RCC_BDCR_LSEDRV>();    	}
		auto LSEBYP() { return base_t::template Actual<RCC_BDCR_LSEBYP>();    	}
		auto LSERDY() { return base_t::template Actual<RCC_BDCR_LSERDY>();    	}
		auto LSEON () { return base_t::template Actual<RCC_BDCR_LSEON>();     	}

		void EnableLSE(DriveCapability const pDrive = DriveCapability::Low)
		{
			LSEDRV() = (std::uint32_t)pDrive;
			LSEON() = true;
			while (!LSERDY());
			LSEBYP() = true;
		}
		void DisableLSE(DriveCapability const pDrive = DriveCapability::Low)
		{
			LSEBYP() = false;
			LSEON() = false;
			while (LSERDY());
		}
	};

	template <std::size_t A>
	struct CSR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto LPWRRSTF() { return base_t::template Actual<RCC_CSR_LPWRRSTF>(); 	}
		auto WWDGRSTF() { return base_t::template Actual<RCC_CSR_WWDGRSTF>(); 	}
		auto IWDGRSTF() { return base_t::template Actual<RCC_CSR_IWDGRSTF>();  	}
		auto SFTRSTF () { return base_t::template Actual<RCC_CSR_SFTRSTF>();  	}
		auto PORRSTF () { return base_t::template Actual<RCC_CSR_PORRSTF>();  	}
		auto PINRSTF () { return base_t::template Actual<RCC_CSR_PINRSTF>();  	}
		auto OBLRSTF () { return base_t::template Actual<RCC_CSR_OBLRSTF>();  	}
		auto RMVF    () { return base_t::template Actual<RCC_CSR_RMVF>();  		}

		//Low Speed oscillator bits
		auto LSIRDY  () { return base_t::template Actual<RCC_CSR_LSIRDY>();  	}
		auto LSION   () { return base_t::template Actual<RCC_CSR_LSION>();  	}

		//LSI Management functions
		void EnableLSI()
		{
			LSION() = true;
			while (!LSIRDY());
		}
		void DisableLSI()
		{
			LSION() = false;
			while(LSIRDY());
		}
	};

	template <std::size_t A>
	struct AHBRSTR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto SCRST   () { return base_t::template Actual<RCC_AHBRSTR_TSCRST>(); 	}
		auto GPIOFRST() { return base_t::template Actual<RCC_AHBRSTR_GPIOFRST>();}
		auto GPIOERST() { return base_t::template Actual<RCC_AHBRSTR_GPIOERST>();}
		auto GPIODRST() { return base_t::template Actual<RCC_AHBRSTR_GPIODRST>();}
		auto GPIOCRST() { return base_t::template Actual<RCC_AHBRSTR_GPIOCRST>();}
		auto GPIOBRST() { return base_t::template Actual<RCC_AHBRSTR_GPIOBRST>();}
		auto GPIOARST() { return base_t::template Actual<RCC_AHBRSTR_GPIOARST>();}
	};

	template <std::size_t A>
	struct APB2RSTR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto DBGMCURST() { return base_t::template Actual<RCC_APB2RSTR_DBGMCURST>(); }
		auto TIM17RST () { return base_t::template Actual<RCC_APB2RSTR_TIM17RST>(); 	}
		auto TIM16RST () { return base_t::template Actual<RCC_APB2RSTR_TIM16RST>(); 	}
		auto TIM15RST () { return base_t::template Actual<RCC_APB2RSTR_TIM15RST>(); 	}
		auto USART1RST() { return base_t::template Actual<RCC_APB2RSTR_USART1RST>(); }
		auto SPI1RST  () { return base_t::template Actual<RCC_APB2RSTR_SPI1RST>(); 	}
		auto TIM1RST  () { return base_t::template Actual<RCC_APB2RSTR_TIM1RST>(); 	}
		auto ADCRST   () { return base_t::template Actual<RCC_APB2RSTR_ADCRST>(); 	}
		auto USART8RST() { return base_t::template Actual<RCC_APB2RSTR_USART8RST>(); }
		auto USART7RST() { return base_t::template Actual<RCC_APB2RSTR_USART7RST>(); }
		auto USART6RST() { return base_t::template Actual<RCC_APB2RSTR_USART6RST>(); }
		auto SYSCFGRST() { return base_t::template Actual<RCC_APB2RSTR_SYSCFGRST>(); }
	};

	template <std::size_t A>
	struct APB1RSTR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto CECRST   () { return base_t::template Actual<RCC_APB1RSTR_CECRST>(); 	}
		auto DACRST   () { return base_t::template Actual<RCC_APB1RSTR_DACRST>(); 	}
		auto PWRRST   () { return base_t::template Actual<RCC_APB1RSTR_PWRRST>(); 	}
		auto CRSRST   () { return base_t::template Actual<RCC_APB1RSTR_CRSRST>(); 	}
		auto CANRST   () { return base_t::template Actual<RCC_APB1RSTR_CANRST>(); 	}
		auto USBRST   () { return base_t::template Actual<RCC_APB1RSTR_USBRST>(); 	}
		auto I2C2RST  () { return base_t::template Actual<RCC_APB1RSTR_I2C2RST>(); 	}
		auto I2C1RST  () { return base_t::template Actual<RCC_APB1RSTR_I2C1RST>(); 	}
		auto USART5RST() { return base_t::template Actual<RCC_APB1RSTR_USART5RST>();}
		auto USART4RST() { return base_t::template Actual<RCC_APB1RSTR_USART4RST>();}
		auto USART3RST() { return base_t::template Actual<RCC_APB1RSTR_USART3RST>();}
		auto USART2RST() { return base_t::template Actual<RCC_APB1RSTR_USART2RST>();}
		auto SPI2RST  () { return base_t::template Actual<RCC_APB1RSTR_SPI2RST>(); 	}
		auto WWDGRST  () { return base_t::template Actual<RCC_APB1RSTR_WWDGRST>(); 	}
		auto TIM14RST () { return base_t::template Actual<RCC_APB1RSTR_TIM14RST>(); }
		auto TIM7RST  () { return base_t::template Actual<RCC_APB1RSTR_TIM7RST>(); 	}
		auto TIM6RST  () { return base_t::template Actual<RCC_APB1RSTR_TIM6RST>(); 	}
		auto TIM3RST  () { return base_t::template Actual<RCC_APB1RSTR_TIM3RST>(); 	}
		auto TIM2RST  () { return base_t::template Actual<RCC_APB1RSTR_TIM2RST>(); 	}
	};

	template <std::size_t A>
	struct CFGR2 : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		//Prescale division
		auto PREDIV	() { return base_t::template Actual<RCC_CFGR2_PREDIV1>(); 	}

		//PREDIV division functions
		void SetPLLDivision(PLLDivision const pInput)
		{
			PREDIV() = (std::uint32_t)pInput;
		}
	};

	template <std::size_t A>
	struct CFGR3 : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto USART1SW() { return base_t::template Actual<RCC_CFGR3_USART1SW>(); 	}
		auto USART2SW() { return base_t::template Actual<RCC_CFGR3_USART2SW>(); 	}
		auto USART3SW() { return base_t::template Actual<RCC_CFGR3_USART3SW>(); 	}
		auto USBSW   () { return base_t::template Actual<RCC_CFGR3_USBSW>(); 	}
		auto CECSW   () { return base_t::template Actual<RCC_CFGR3_CECSW>(); 	}
		auto I2C1SW  () { return base_t::template Actual<RCC_CFGR3_I2C1SW>(); 	}

		//This is an obselete field, and is diabled for that reason
		//auto ADCSW   () { return Actual<RCC_CFGR3_ADCSW>(); 	}

		template<std::uint32_t Channel>
		void SelectUSARTClock(USARTClock const pInput)
		{
			if constexpr (Channel == 1)
				USART1SW() = (std::uint32_t)pInput;

			if constexpr (Channel == 2)
				USART2SW() = (std::uint32_t)pInput;

			if constexpr (Channel == 3)
				USART3SW() = (std::uint32_t)pInput;
		}
		void SelectUSBClock(USBClock const pInput)
		{
			USBSW() = (std::uint32_t)pInput;
		}
		void SelectHDMIClock(HDMIClock const pInput)
		{
			CECSW() = (std::uint32_t)pInput;
		}
		void I2CClockSource(I2CClock const pInput)
		{
			I2C1SW() = (std::uint32_t)pInput;
		}
		I2CClock I2CClockSource()
		{
			return (I2CClock)(bool)I2C1SW();
		}
	};

	template <std::size_t A>
	struct CR2 : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		//High speed Internal 48MHz clock
		auto HSI48CAL () { return base_t::template Actual<RCC_CR2_HSI48CAL>(); 	}
		auto HSI48RDY () { return base_t::template Actual<RCC_CR2_HSI48RDY>(); 	}
		auto HSI48ON  () { return base_t::template Actual<RCC_CR2_HSI48ON>(); 	}

		//HSI48 Management functions
		void EnableHSI48()
		{
			HSI48ON() = true;
			while (!HSI48RDY());
		}
		void DisableHSI48()
		{
			HSI48ON() = false;
			while (HSI48RDY());
		}

		//High speed internal 14MHz clock
		auto HSI14CAL () { return base_t::template Actual<RCC_CR2_HSI14CAL>(); 	}
		auto HSI14TRIM() { return base_t::template Actual<RCC_CR2_HSI14TRIM>(); }
		auto HSI14RDY () { return base_t::template Actual<RCC_CR2_HSI14RDY>(); 	}
		auto HSI14ON  () { return base_t::template Actual<RCC_CR2_HSI14ON>(); 	}

		//Problematic register, hidden from interface, uncomment to enable
		//auto HSI14DIS () { return Actual<RCC_CR2_HSI14DIS>(); }

		//HSI14 Management functions
		void EnableHSI14()
		{
			HSI14ON() = true;
			while(!HSI14RDY());
		}
		void DisableHSI14()
		{
			HSI14ON() = false;
			while(HSI14RDY());
		}
	};

	//Defaults;
	CR() 		-> CR		<RCC_BASE + offsetof(RCC_TypeDef, CR)>;
	CFGR() 		-> CFGR		<RCC_BASE + offsetof(RCC_TypeDef, CFGR)>;
	CIR() 		-> CIR		<RCC_BASE + offsetof(RCC_TypeDef, CIR)>;
	AHBENR()	-> AHBENR	<RCC_BASE + offsetof(RCC_TypeDef, AHBENR)>;
	APB2ENR() 	-> APB2ENR	<RCC_BASE + offsetof(RCC_TypeDef, APB2ENR)>;
	APB1ENR() 	-> APB1ENR	<RCC_BASE + offsetof(RCC_TypeDef, APB1ENR)>;
	BDCR() 		-> BDCR		<RCC_BASE + offsetof(RCC_TypeDef, BDCR)>;
	CSR() 		-> CSR		<RCC_BASE + offsetof(RCC_TypeDef, CSR)>;
	AHBRSTR() 	-> AHBRSTR	<RCC_BASE + offsetof(RCC_TypeDef, AHBRSTR)>;
	APB2RSTR() 	-> APB2RSTR	<RCC_BASE + offsetof(RCC_TypeDef, APB2RSTR)>;
	APB1RSTR() 	-> APB1RSTR	<RCC_BASE + offsetof(RCC_TypeDef, APB1RSTR)>;
	CFGR2() 	-> CFGR2	<RCC_BASE + offsetof(RCC_TypeDef, CFGR2)>;
	CFGR3() 	-> CFGR3	<RCC_BASE + offsetof(RCC_TypeDef, CFGR3)>;
	CR2() 		-> CR2		<RCC_BASE + offsetof(RCC_TypeDef, CR2)>;
}