#pragma once

#include "StaticLambdaWrapper.hpp"
#include "RCC.hpp"
#include "PWRRegisters.hpp"
#include "InterruptRegisters.hpp"
#include <algorithm>

namespace Peripherals
{
    /**
     * @brief Manages the power on and off of the PWR module.
     */
	struct PWRPowerKernal
	{
		static constexpr auto ChannelInterrupt = System::InterruptSource::ePVD_VDDIO2;
        /**
         * @brief Powers on the PWR peripheral and sets up enables the NVIC interrupt.
         */
		static void Construct() noexcept
		{
            using namespace System::InterruptGeneral;

            //Enable module
            RCCGeneral::APB1ENR{}.PWREN() = true;

            //Enable rising edge triggers and the corresponding interrupt
            IMR<16>{}.IM() = true;
            RTSR<16>{}.RT() = true;

            //Enable interrupt
			NVIC_EnableIRQ((IRQn_Type)ChannelInterrupt);
			NVIC_SetPriority((IRQn_Type)ChannelInterrupt, 0);
		}
        /**
         * @brief Disables the interrupt in NVIC and powers down the PWR peripheral. 
         */
		static void Destruct() noexcept
        {
            using namespace System::InterruptGeneral;

            //Disable interrupt
			NVIC_DisableIRQ((IRQn_Type)ChannelInterrupt);

            //Enable module
            RCCGeneral::APB1ENR{}.PWREN() = false;
        }
	};
    /**
     * @brief The PWR module helper function.
     * 
     * @tparam F The function callback.
     */
    template <typename F>
    class PWRModule : Power::Power<PWRPowerKernal>, System::Interrupt<PWRModule<F>, System::InterruptSource::ePVD_VDDIO2>
    {
    private:
        using callback_t = General::StaticLambdaWrapper<F>;
		callback_t m_Callback;
    public:
        /**
         * @brief Construct a new PWRModule object
         * 
         * @tparam C The callback type. 
         * @param callback The callback to run when the threshold is reached (low voltage).
         * @param threshold The voltage threshold to raise an alert.
         */
        template <typename C>
        PWRModule(C && callback, float threshold = 3.0f) noexcept: 
            m_Callback{ std::forward<C>(callback) }
        {
            constexpr float     PLS_min     = 2.2;
            constexpr float     PLS_max     = 3.0;
            constexpr float     PLS_delta   = PLS_max - PLS_min;
            constexpr float     PLS_full    = 7.0;
            std::uint32_t const PLS_bits    = (std::uint32_t)(((std::clamp(threshold, PLS_min, PLS_max) - PLS_min) / PLS_delta) * PLS_full);
            
            // PVD( programmable voltage detector )
            PWRGeneral::CR().PLS()  = PLS_bits;     //Configure for the correct threshold
            PWRGeneral::CR().PVDE() = true;         //Enable PVD
        }
        /**
         * @brief The interrupt handler for the PWR peripheral.
         */
        static void Interrupt() noexcept
        {
            callback_t::Run();
        }
    };
    /**
     * @brief A deduction guide for the PWR peripheral
     * 
     * @tparam F The interrupt callback.
     */
    template <typename F>
    PWRModule(F) -> PWRModule<F>;
    /**
     * @brief A deduction guide for the PWR peripheral
     * 
     * @tparam F The interrupt callback.
     */
    template <typename F>
    PWRModule(F, float) -> PWRModule<F>;
}