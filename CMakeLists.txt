cmake_minimum_required(VERSION 3.9)
include(CMake/Toolchain.cmake)


# Configure the C and C++ standards
SET(CMAKE_C_STANDARD 11)
SET(CMAKE_CXX_STANDARD 17)

project(uSupplyUSB)
enable_language(C)
enable_language(CXX)
enable_language(ASM)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# first we can indicate the documentation build as an option and set it to ON by default
option(BUILD_DOC "Build documentation" ON)

# SETUP PROJECT HEADERS
include_directories(
	"include"
	"system/"
	"library/"
	"library/usb/"
	"library/parts/"
	"library/usb/HID"
	"library/usb/LL"
	"library/usb/Standard"
	"library/usb/Types"
	"library/common/"
	"library/common/SCPI/"
	"library/common/containers"
	"library/peripherals"
	"library/bitbanging"
	"library/peripherals/USART"
	"richtek"
	"st-library/include/"
	"st-library/include/arm"
	"st-library/include/cmsis"
	"st-library/include/cortexm"
	"st-library/include/diag"
	"st-library/include/stm32f0-stdperiph"
)

#Add all linker scripts to the linker command
file(GLOB_RECURSE
	LK_SCRIPTS
	"*.ld"
)
foreach(file ${LK_SCRIPTS})
	set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -T \"${file}\"")
endforeach()

#SETUP PROJECT SOURCES
file(GLOB_RECURSE 
	 MAIN_SOURCE
	"st-library/*.c"
	"st-library/*.cpp" 
	"st-library/newlib/*.c"
	"src/*.c"
	"src/*.cpp"
	"richtek/*.cpp"
	"richtek/src/*.c"
)
#
# Including extra cmake rules
#include(cmake/ClangTidy.cmake)

#Compile and link the exe :)
add_compile_definitions(TRACE TYPEC_ROLE_SNK_ENABLE)


add_library( richtek_library STATIC IMPORTED )
set_property( TARGET richtek_library PROPERTY IMPORTED_LOCATION ${PROJECT_SOURCE_DIR}/richtek/lib/libbuild.a )
include_directories( "richtek/inc" )

add_executable( ${TARGET}.elf ${MAIN_SOURCE} )
target_link_libraries( ${TARGET}.elf richtek_library )

#Print the size of the .hex
add_custom_target(size ALL arm-none-eabi-size ${TARGET}.elf DEPENDS ${TARGET}.elf)
add_custom_target(${TARGET}.bin ALL DEPENDS ${TARGET}.elf COMMAND ${CMAKE_OBJCOPY} -Obinary ${TARGET}.elf ${TARGET}.bin)


# check if Doxygen is installed
find_package(Doxygen)
if (DOXYGEN_FOUND)
	if (CMAKE_BUILD_TYPE MATCHES "^[Rr]elease")
		# set input and output files
		set(DOXYGEN_IN doxygen/Doxyfile.in)
		set(DOXYGEN_OUT Doxyfile)
        set(DOXY_BASE_DIR ${CMAKE_HOME_DIRECTORY} CACHE PATH ${CMAKE_HOME_DIRECTORY})

		# request to configure the file
		configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
		message("Doxygen build started")

		# note the option ALL which allows to build the docs together with the application
        add_custom_target(
            doc ALL
            COMMAND
                ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/doxyfile
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                COMMENT "Generating API documentation with Doxygen"
                VERBATIM
        )
	endif()
else()
	message("If you want doxygen documents, you need to install doxygen.")
endif ()