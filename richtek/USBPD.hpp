#pragma once
#include "Constants.hpp"

extern volatile bool TypeCMode;

namespace USB::PD
{
    void Tick() noexcept;
    void Run() noexcept;
    void Start() noexcept;
    /**
     * @brief 
     * 
     */
    struct USBPD
    {
    private:
        /**
         * @brief Used to store a single power point in the type c port.
         */
        struct TypeCPowerCommands
        {
            float const Minimum, Maximum;
            const char * Command;
        };
        /**
         * @brief A lookup table of the power limits given a specific measured voltage.
         */
        static constexpr TypeCPowerCommands TypeCBoundaries[]
        {
            { 4.5f, 	5.5f,   "!SOUR:POW:LIM 7!" },
            { 8.5f, 	9.5f,   "!SOUR:POW:LIM 10!" },
            { 11.0f, 	13.f,   "!SOUR:POW:LIM 12!" },
            { 14.0f, 	16.f,   "!SOUR:POW:LIM 14!" }
        };
    public:
        /**
         * @brief Construct a new USBPD object
         */
        inline USBPD() noexcept
        {
            Start();
        }
        /**
         * @brief Gets whether or not TypeC power mode is enabled.
         * 
         * @return true 
         * @return false 
         */
        inline static bool TypeCPowerMode() noexcept
        {
            return TypeCMode;
        }
        /**
         * @brief Retrieves the command for the power limit of the connection.
         * 
         * @param v_bus The measured v_bus voltage.
         */
        inline static const char * GetPowerLimitCommand( float v_bus ) noexcept
        {
            for ( auto const & type : TypeCBoundaries )
            {
                // is present at all.
                if ( General::Between( v_bus, type.Minimum, type.Maximum ) )
                    return type.Command;
            }
            //
            return nullptr;
        }
        /**
         * @brief This is the interrupt called by the GPIO interrupt occured.
         */
        inline static void Poll() noexcept
        {
            Run();
        }
    };
}