
/*     Copyright <2017> <Richtek Technology Corp.>
        Permission is hereby granted, free of charge, to any person obtaining a copy of
        this source code, library, and associated documentation files( the "Software"),
        to deal in the source code without restriction, including without limitation the
        rights to use, copy, modify, merge, publish, distribute copies of the
        Software, and to permit persons to whom the Software is furnished to do so,
        subject to the following conditions:
        The Software is restricted to use with Richtek's USB PD products or evaluation kits.
        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
        FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
        COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
        IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
        CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef PD_EXPORT_DEF_H
#define PD_EXPORT_DEF_H


/* Power role */
#define PD_POWER_ROLE_SINK   1
#define PD_POWER_ROLE_SOURCE 0

/* Data role */
#define PD_DATA_ROLE_UFP    1
#define PD_DATA_ROLE_DFP    0

/* USB-IF SIDs */
#define USB_SID_PD          		0xFF00 	/* power delivery */
#define USB_SID_DISPLAYPORT 	0xFF01	/* display port */

/*PDO define*/
#define PDO_TYPE_FIX (0)
#define PDO_TYPE_BAT (1)
#define PDO_TYPE_VAR (2)
#define PDO_TYPE_AUG (3)

#define	PDO_VOLTAGE(x)		(((uint16_t)x)/50)
#define	PDO_CURRENT(x)		(((uint16_t)x)/10)
#define	PDO_POWER(x)		(((uint16_t)x)/250)


#define	PDO_AUG_VOLTAGE(x)		(((uint16_t)x)/100)
#define	PDO_AUG_CURRENT(x)		(((uint16_t)x)/50)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define	SRC_CAP_PDO_B3( flags)			((PDO_TYPE_FIX<<6)+((flags)<<1)+0x00)
#define	SRC_CAP_PDO_B2(peak, voltage)		(((peak)<<4)+((((uint16_t)voltage)&0x03C0)>>6))
#define	SRC_CAP_PDO_B1(voltage, max_curr)	(((((uint16_t)voltage)&0x003F)<<2)+((((uint16_t)max_curr)&0x0300)>>8))
#define	SRC_CAP_PDO_B0(max_curr)		(((uint16_t)max_curr)&0x00FF)

#define	SNK_CAP_PDO_B3( flags)			((PDO_TYPE_FIX<<6)+(flags<<1)+0x00)
#define	SNK_CAP_PDO_B2( voltage)		((0x00<<4)+((((uint16_t)voltage)&0x03C0)>>6))
#define	SNK_CAP_PDO_B1(voltage, max_curr)	(((((uint16_t)voltage)&0x003F)<<2)+((((uint16_t)max_curr)&0x0300)>>8))
#define	SNK_CAP_PDO_B0(max_curr)		(((uint16_t)max_curr)&0x00FF)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define	PDO_BAT_B3( max_vol)			((PDO_TYPE_BAT<<6)+((((uint16_t)max_vol)&0x03F0) >>4))
#define	PDO_BAT_B2(max_vol, min_vol)		(((((uint16_t)max_vol)&0x000F) <<4) + ((((uint16_t)min_vol)&0x03C0) >>6)	)
#define	PDO_BAT_B1(min_vol, pwr)		(((((uint16_t)min_vol)&0x003F) <<2) + ((((uint16_t)pwr)&0x0300) >>8))
#define	PDO_BAT_B0(pwr)				((((uint16_t)pwr)&0x000FF) )

#define	PDO_VAR_B3( max_vol)			((PDO_TYPE_VAR<<6)+((((uint16_t)max_vol)&0x03F0) >>4))
#define	PDO_VAR_B2(max_vol, min_vol)		(((((uint16_t)max_vol)&0x000F) <<4) + ((((uint16_t)min_vol)&0x03C0) >>6)	)
#define	PDO_VAR_B1(min_vol, curr)		(((((uint16_t)min_vol)&0x003F) <<2) + ((((uint16_t)curr)&0x0300) >>8))
#define	PDO_VAR_B0(curr)					((((uint16_t)curr)&0x000FF) )

#define PDO_AUG_TYPE_PPS	(0)

#define PDO_PPS_B3(max_vol)					((PDO_TYPE_AUG<<6)+(PDO_AUG_TYPE_PPS<<4)+(((uint16_t)max_vol&0x200)>>9))
#define PDO_PPS_B2(max_vol)						((max_vol&0x7F)<<1)
#define PDO_PPS_B1(min_vol)							(min_vol&0xFF)
#define PDO_PPS_B0(curr)							(curr&0x7F)

#define EXTRACT_PDO_TYPE_FROM_B3(b3)			(((b3)>>6)&0x03)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////						SRC/SNK Capabilities Macros															/////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define	SRC_CAP_5V_PDO(voltage, max_curr,cap_flag)						SRC_CAP_PDO_B0(PDO_CURRENT(max_curr)), SRC_CAP_PDO_B1(PDO_VOLTAGE(voltage), PDO_CURRENT(max_curr)), SRC_CAP_PDO_B2(0x00, PDO_VOLTAGE(voltage)), SRC_CAP_PDO_B3(cap_flag)
#define	SRC_CAP_FIX_PDO(voltage, max_curr)						SRC_CAP_PDO_B0(PDO_CURRENT(max_curr)), SRC_CAP_PDO_B1(PDO_VOLTAGE(voltage), PDO_CURRENT(max_curr)), SRC_CAP_PDO_B2(0x00, PDO_VOLTAGE(voltage)), SRC_CAP_PDO_B3(0x00)
#define	SRC_CAP_BAT_PDO(max_vol, min_vol, pwr)				PDO_BAT_B0(PDO_POWER(pwr)), PDO_BAT_B1(PDO_VOLTAGE(min_vol), PDO_POWER(pwr)), PDO_BAT_B2(PDO_VOLTAGE(max_vol), PDO_VOLTAGE(min_vol)), PDO_BAT_B3(, PDO_VOLTAGE(max_vol))
#define	SRC_CAP_VAR_PDO(max_vol, min_vol, curr)				PDO_VAR_B0(PDO_CURRENT(curr)), PDO_VAR_B1(PDO_VOLTAGE(min_vol), PDO_CURRENT(curr)), PDO_VAR_B2(PDO_VOLTAGE(max_vol), PDO_VOLTAGE(min_vol)), PDO_VAR_B3( PDO_VOLTAGE(max_vol))

#define	SNK_CAP_5V_PDO(voltage, max_curr,cap_flag)						SNK_CAP_PDO_B0(PDO_CURRENT(max_curr)), SNK_CAP_PDO_B1(PDO_VOLTAGE(voltage), PDO_CURRENT(max_curr)), SNK_CAP_PDO_B2( PDO_VOLTAGE(voltage)), SNK_CAP_PDO_B3(cap_flag)
#define	SNK_CAP_FIX_PDO(voltage, max_curr)					SNK_CAP_PDO_B0(PDO_CURRENT(max_curr)), SNK_CAP_PDO_B1(PDO_VOLTAGE(voltage), PDO_CURRENT(max_curr)), SNK_CAP_PDO_B2( PDO_VOLTAGE(voltage)), SNK_CAP_PDO_B3(0x00)
#define	SNK_CAP_BAT_PDO(max_vol, min_vol, pwr)				PDO_BAT_B0(PDO_POWER(pwr)), PDO_BAT_B1(PDO_VOLTAGE(min_vol), PDO_POWER(pwr)), PDO_BAT_B2(PDO_VOLTAGE(max_vol), PDO_VOLTAGE(min_vol)), PDO_BAT_B3(PDO_VOLTAGE(max_vol))
#define	SNK_CAP_VAR_PDO(max_vol, min_vol, curr)				PDO_VAR_B0(PDO_CURRENT(curr)), PDO_VAR_B1(PDO_VOLTAGE(min_vol), PDO_CURRENT(curr)), PDO_VAR_B2(PDO_VOLTAGE(max_vol), PDO_VOLTAGE(min_vol)), PDO_VAR_B3( PDO_VOLTAGE(max_vol))


#define SNK_CAP_AUG_PDO( min_vol,max_vol, curr)	PDO_PPS_B0(PDO_AUG_CURRENT(curr)),PDO_PPS_B1(PDO_AUG_VOLTAGE(min_vol)),PDO_PPS_B2(PDO_AUG_VOLTAGE(max_vol)),PDO_PPS_B3(PDO_AUG_VOLTAGE(max_vol))


#endif

