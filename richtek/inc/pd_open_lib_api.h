/*     Copyright <2017> <Richtek Technology Corp.>
        Permission is hereby granted, free of charge, to any person obtaining a copy of
        this source code, library, and associated documentation files( the "Software"),
        to deal in the source code without restriction, including without limitation the
        rights to use, copy, modify, merge, publish, distribute copies of the
        Software, and to permit persons to whom the Software is furnished to do so,
        subject to the following conditions:
        The Software is restricted to use with Richtek's USB PD products or evaluation kits.
        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
        FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
        COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
        CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef	PD_OPEN_LIB_API_H
#define	PD_OPEN_LIB_API_H

#ifdef __cplusplus
extern "C"{
#endif

typedef   signed          char int8_t;

typedef   signed short     int int16_t;

//typedef   signed           int int32_t;

typedef   signed       long long int64_t;

typedef unsigned          char uint8_t;

typedef unsigned short     int uint16_t;

//typedef unsigned           int uint32_t;

typedef unsigned       long long uint64_t;

typedef   signed          char int_least8_t;

typedef   signed short     int int_least16_t;

//typedef   signed           int int_least32_t;

typedef   signed       long long int_least64_t;

typedef unsigned          char uint_least8_t;

typedef unsigned short     int uint_least16_t;

//typedef unsigned           int uint_least32_t;

typedef unsigned       long long uint_least64_t;

typedef   signed           int int_fast8_t;

typedef   signed           int int_fast16_t;

typedef   signed           int int_fast32_t;

typedef   signed       long long int_fast64_t;

typedef unsigned           int uint_fast8_t;

typedef unsigned           int uint_fast16_t;

typedef unsigned           int uint_fast32_t;

typedef unsigned       long long uint_fast64_t;

typedef   signed           int intptr_t;

typedef unsigned           int uintptr_t;

typedef   signed     long long intmax_t;

typedef unsigned     long long uintmax_t;

typedef unsigned char xu8;

typedef unsigned short xu16;

typedef unsigned int xu32;

typedef unsigned char iu8;

typedef unsigned short iu16;

typedef unsigned int iu32;

typedef unsigned char *ptr8;

typedef unsigned short *ptr16 ;

typedef union _xdata_bits8

{

	uint8_t val;

	struct

	{

		uint8_t bit0:      1;  

		uint8_t bit1:      1;  

		uint8_t bit2:      1;  

		uint8_t bit3:      1;  

		uint8_t bit4:      1;  

		uint8_t bit5:      1;  

		uint8_t bit6:      1;  

		uint8_t bit7:      1;  

	}__attribute__((packed)) bits;

}xbits8_t;

typedef union _xdata_bits16

{

    uint16_t val;

    struct

    {

    	uint16_t bit0:      1;  

       uint16_t bit1:      1;  

       uint16_t bit2:      1;  

       uint16_t bit3:      1;  

       uint16_t bit4:      1;  

       uint16_t bit5:      1;  

       uint16_t bit6:      1;  

       uint16_t bit7:      1;  

    	uint16_t bit8:      1;  

       uint16_t bit9:      1;  

       uint16_t bit10:      1;  

       uint16_t bit11:      1;  

       uint16_t bit12:      1;  

       uint16_t bit13:      1;  

       uint16_t bit14:      1;  

       uint16_t bit15:      1;  

    }__attribute__((packed)) bits;

}xbits16_t;

typedef void ( *func_ptr_void_void)(void);

typedef _Bool ( *func_ptr_bool_void)(void);

typedef _Bool ( *func_ptr_bool_u8)(uint8_t);

typedef _Bool ( *func_ptr_bool_p8)(uint8_t*);

typedef void ( *func_ptr_void_p8)(uint8_t*);

typedef void ( *func_ptr_void_u8)(uint8_t);

typedef _Bool ( *func_ptr_bool_u16)(uint16_t);

/*    The following functions need to be implemented                                       */

extern void irq_lock(void);

extern void irq_unlock(void);

extern _Bool plat_i2c_read_block(uint8_t slave,uint8_t reg, uint8_t Cnt, uint8_t *pData);

extern _Bool plat_i2c_write_block(uint8_t slave,uint8_t reg, uint8_t Cnt, uint8_t *pData);

extern _Bool plat_check_alert(void);

extern _Bool plat_check_vdc_in(void);

extern uint8_t  slave_base_addr;

enum {

    TYPEC_ROLE_SNK = 0,

    TYPEC_ROLE_SRC,

    TYPEC_ROLE_DRP,

    TYPEC_ROLE_TRY_SRC,

    TYPEC_ROLE_TRY_SNK,

    TYPEC_ROLE_NR,

};

extern const uint8_t  src_cap_cnt[1];

extern const uint8_t  src_default_caps_tbl[1][28];

extern uint8_t snk_cap_cnt[1];

extern uint8_t snk_default_caps_tbl_le[1][28];

extern _Bool pd_dpm_prs_evaluate_swap(void);

extern _Bool pd_dpm_drs_evaluate_swap(void);

extern _Bool pd_dpm_vcs_evaluate_swap(void);

extern _Bool dpm_evaluate_src_transition_supply_gotomin(void);

extern xbits8_t dpm_config_flags[1];

extern _Bool pd_allow_time_consuming_task;

extern void PD_Reset_Init(void);

extern void PD_Run_State_Machine(void);

extern void Timer_1Ms_Handler(void);

extern uint16_t Get_Timestamp(void);

extern void Notify_PD_Shutdown(void);

extern void Notify_PD_Sleep(void);

extern void Notify_PD_Wakeup(void);
enum{

NOTIFY_TCPM_STATUS_DETACH,

NOTIFY_TCPM_STATUS_ATTACH,

NOTIFY_TCPM_STATUS_READY,

};

enum{

	DPM_COMMAND_NULL=0,

	DPM_COMMAND_SRC_CAP_CHANGE,

	DPM_COMMAND_GET_SNK_CAP,

	DPM_COMMAND_GET_SRC_CAP,

	DPM_COMMAND_SEND_SOFTRESET,

	DPM_COMMAND_PR_SWAP,

	DPM_COMMAND_DR_SWAP,

	DPM_COMMAND_VCONN_SWAP,

	DPM_COMMAND_VDM_REQ,

	DPM_COMMAND_VDM_SEND,

	DPM_COMMAND_NUMBER

};

enum{

	DPM_EVENT_STATUS_IDLE,

	DPM_EVENT_STATUS_WAIT_HANDLE,

	DPM_EVENT_STATUS_BUSY,

	DPM_EVENT_STATUS_SUCCESS,

};

typedef struct{

	uint8_t cnt;

	uint8_t buff[28];

}sDpmPdo_t;
extern volatile bool TypeCMode;

typedef union{

	uint8_t buffer[32];

	sDpmPdo_t pdo;

	uint8_t accept;

}uIndvArg_t;

typedef struct{

	uint8_t status;

	uIndvArg_t arg;

}sDpmCbArg_t;

typedef void ( *func_ptr_void_pCb)(sDpmCbArg_t*);

enum tcpm_transmit_type {

	TCPC_TX_SOP = 0,

	TCPC_TX_SOP_P = 1,

	TCPC_TX_SOP_PP = 2,

	TCPC_TX_SOP_DEBUG_PRIME = 3,

	TCPC_TX_SOP_DEBUG_PRIME_PRIME = 4,

	TCPC_TX_HARD_RESET = 5,

	TCPC_TX_CABLE_RESET = 6,

	TCPC_TX_BIST_MODE_2 = 7,

};

typedef struct{

	uint8_t sop_type;

	uint8_t vdm_header[4];

	uint8_t vdm_cnt;

	uint8_t *vdm_p_buffer;

}sVdmRequestArg_t;

extern sVdmRequestArg_t vdm_request_info[1];

extern _Bool Send_Dpm_Command(uint8_t port,uint8_t cmd,func_ptr_void_pCb pCallback);

extern xu8 power_role[1];

extern xu8 data_role[1];

extern xbits8_t		pd_flags[1];

extern xbits8_t		typec_flags[1];

#endif


#ifdef __cplusplus
}
#endif