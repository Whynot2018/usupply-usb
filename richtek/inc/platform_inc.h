/*     Copyright <2017> <Richtek Technology Corp.>
        Permission is hereby granted, free of charge, to any person obtaining a copy of
        this source code, library, and associated documentation files( the "Software"),
        to deal in the source code without restriction, including without limitation the
        rights to use, copy, modify, merge, publish, distribute copies of the
        Software, and to permit persons to whom the Software is furnished to do so,
        subject to the following conditions:
        The Software is restricted to use with Richtek's USB PD products or evaluation kits.
        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
        FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
        COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
        IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
        CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#ifndef PLATFORM_INC_H
#define PLATFORM_INC_H

#include "stdbool.h"
#include "pd_open_lib_api.h"
#include "pd_export_def.h"

#endif